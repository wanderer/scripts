#!/bin/bash

# this little script backs up all the config files that matter

dotfiles=~/utils/dotfiles
MEGA="MEGA/Private"

params="auP"
if [ "$1" == "-d"  ]; then
    params="aunP" # rsync -n for dry run
fi

rsync -$params /etc/X11/Xresources $dotfiles/etc/X11

rsync -$params ~/alltheconfigs.sh $dotfiles

rsync -$params ~/.vimrc $dotfiles/vimrc
#rsync -$params ~/.gvimrc $dotfiles/gvimrc really using just vimrc
rsync -$params ~/.zshrc $dotfiles/zsh/zshrc

#rsync -$params ~/$MEGA/backups/authenticator.sh $dotfiles/backups
rsync -$params ~/$MEGA/backups/createarchive.sh $dotfiles/backups

rsync -$params ~/$MEGA/fedora-backup/fedora-setup/fedora-setup.sh $dotfiles/setup-env
rsync -$params ~/$MEGA/fedora-backup/fedora-setup/deploy-dotfiles.sh $dotfiles/setup-env
rsync -$params ~/$MEGA/fedora-backup/fedora-setup/EASetup.sh $dotfiles/setup-env

rsync -$params ~/.oh-my-zsh/themes/myowntheme* $dotfiles/zsh/oh-my-zsh/themes
