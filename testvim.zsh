#!/bin/zsh

set -xe

nutmp=$(mktemp -d)
cd "$nutmp"
unset nutmp

for i in {1..20}; do
    vim --startuptime start$i.log;
done

find start* -exec grep STARTED {} \; | cut -d' ' -f1

